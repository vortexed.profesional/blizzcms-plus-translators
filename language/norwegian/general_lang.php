<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Browser Tab Menu*/
$lang['tab_news'] = 'Nyheter';
$lang['tab_forum'] = 'Forum';
$lang['tab_store'] = 'Butikk';
$lang['tab_bugtracker'] = 'Bugs';
$lang['tab_changelogs'] = 'Endringslogg';
$lang['tab_pvp_statistics'] = 'PvP Statistikk';
$lang['tab_login'] = 'Logg Inn';
$lang['tab_register'] = 'Registrer';
$lang['tab_home'] = 'Hjem';
$lang['tab_donate'] = 'Donere';
$lang['tab_vote'] = 'Stemme';
$lang['tab_cart'] = 'Handlevogn';
$lang['tab_account'] = 'Min Konto';
$lang['tab_reset'] = 'Passordgjenoppretting';
$lang['tab_error'] = 'Error 404';
$lang['tab_maintenance'] = 'Vedlikehold';
$lang['tab_online'] = 'Online Spillere';

/*Panel Navbar*/
$lang['navbar_vote_panel'] = 'Stemme Panel';
$lang['navbar_donate_panel'] = 'Donasjons Panel';

/*Button Lang*/
$lang['button_register'] = 'Registrer';
$lang['button_login'] = 'Logg Inn';
$lang['button_logout'] = 'Logg Ut';
$lang['button_forgot_password'] = 'Glemt Passord?';
$lang['button_user_panel'] = 'Bruker Panel';
$lang['button_admin_panel'] = 'Admin Panel';
$lang['button_mod_panel'] = 'Mod Panel';
$lang['button_change_avatar'] = 'Forandre Avatar';
$lang['button_add_personal_info'] = 'Legg Til Personlig Info';
$lang['button_create_report'] = 'Lag Rapport';
$lang['button_new_topic'] = 'Nytt Emne';
$lang['button_edit_topic'] = 'Endre Emne';
$lang['button_save_changes'] = 'Lagre Endringer';
$lang['button_cancel'] = 'Avbryt';
$lang['button_send'] = 'Send';
$lang['button_read_more'] = 'Les Mer';
$lang['button_add_reply'] = 'Legg Til Svar';
$lang['button_remove'] = 'Fjern';
$lang['button_create'] = 'Lag';
$lang['button_save'] = 'Lagre';
$lang['button_close'] = 'Steng';
$lang['button_reply'] = 'Svar';
$lang['button_donate'] = 'Doner';
$lang['button_account_settings'] = 'Konto Innstillinger';
$lang['button_cart'] = 'Legg i Handlevogn';
$lang['button_view_cart'] = 'Vis Handlevogn';
$lang['button_checkout'] = 'Utsjekk';
$lang['button_buying'] = 'Fortsett og Handle';

/*Alert Lang*/
$lang['alert_successful_purchase'] = 'Utstyr Kjøpt Vellykket.';
$lang['alert_upload_error'] = 'Ditt bilde må være JPEG eller PNG format';
$lang['alert_changelog_not_found'] = 'Serveren har ikke endringslogger å informere på nåværende tidspunkt';
$lang['alert_points_insufficient'] = 'Utilstrekkelige poeng';

/*Status Lang*/
$lang['offline'] = 'Avlogget';
$lang['online'] = 'Pålogget';

/*Label Lang*/
$lang['label_open'] = 'Åpen';
$lang['label_closed'] = 'Stengt';

/*Form Label Lang*/
$lang['label_login_info'] = 'Innloggings Info';

/*Input Placeholder Lang*/
$lang['placeholder_username'] = 'Brukernavn';
$lang['placeholder_email'] = 'Epost Addresse';
$lang['placeholder_password'] = 'Passord';
$lang['placeholder_re_password'] = 'Gjenta Passord';
$lang['placeholder_current_password'] = 'Nåværende Passord';
$lang['placeholder_new_password'] = 'Nytt Passord';
$lang['placeholder_new_email'] = 'Ny Epost';
$lang['placeholder_confirm_email'] = 'Bekreft Ny Epost';
$lang['placeholder_create_bug_report'] = 'Lag Bug Rapport';
$lang['placeholder_title'] = 'Tittel';
$lang['placeholder_type'] = 'Type';
$lang['placeholder_description'] = 'Forklaring';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_uri'] = 'Vennlig URL (Eksempel: tos)';
$lang['placeholder_highl'] = 'Fremheve';
$lang['placeholder_lock'] = 'Steng';
$lang['placeholder_subject'] = 'Emne';

/*Table header Lang*/
$lang['table_header_name'] = 'Navn';
$lang['table_header_faction'] = 'Fraksjon';
$lang['table_header_total_kills'] = 'Totalt Drap';
$lang['table_header_today_kills'] = 'Dagens Drap';
$lang['table_header_yersterday_kills'] = 'Gårsdagens Drap';
$lang['table_header_team_name'] = 'Lag Navn';
$lang['table_header_members'] = 'Medlem';
$lang['table_header_rating'] = 'Vurdering';
$lang['table_header_games'] = 'Spill';
$lang['table_header_id'] = 'ID';
$lang['table_header_status'] = 'Status';
$lang['table_header_priority'] = 'Prioritet';
$lang['table_header_date'] = 'Dato';
$lang['table_header_author'] = 'Forfatter';
$lang['table_header_time'] = 'Tid';
$lang['table_header_icon'] = 'Ikon';
$lang['table_header_realm'] = 'Realm';
$lang['table_header_zone'] = 'Sone';
$lang['table_header_character'] = 'Karakter';
$lang['table_header_price'] = 'Pris';
$lang['table_header_item_name'] = 'Utstyrs Navn';
$lang['table_header_items'] = 'Utstyr';
$lang['table_header_quantity'] = 'Mengde';

/*Class Lang*/
$lang['class_warrior'] = 'Warrior';
$lang['class_paladin'] = 'Paladin';
$lang['class_hunter'] = 'Hunter';
$lang['class_rogue'] = 'Rogue';
$lang['class_priest'] = 'Priest';
$lang['class_dk'] = 'Death Knight';
$lang['class_shamman'] = 'Shamman';
$lang['class_mage'] = 'Mage';
$lang['class_warlock'] = 'Warlock';
$lang['class_monk'] = 'Monk';
$lang['class_druid'] = 'Druid';
$lang['class_demonhunter'] = 'Demon Hunter';

/*Faction Lang*/
$lang['faction_alliance'] = 'Allianse';
$lang['faction_horde'] = 'Horde';

/*Gender Lang*/
$lang['gender_male'] = 'Mann';
$lang['gender_female'] = 'Kvinne';

/*Race Lang*/
$lang['race_human'] = 'Human';
$lang['race_orc'] = 'Orc';
$lang['race_dwarf'] = 'Dwarf';
$lang['race_night_elf'] = 'Night Elf';
$lang['race_undead'] = 'Undead';
$lang['race_tauren'] = 'Tauren';
$lang['race_gnome'] = 'Gnome';
$lang['race_troll'] = 'Troll';
$lang['race_goblin'] = 'Goblin';
$lang['race_blood_elf'] = 'Blood Elf';
$lang['race_draenei'] = 'Draenei';
$lang['race_worgen'] = 'Worgen';
$lang['race_panda_neutral'] = 'Pandaren Neutral';
$lang['race_panda_alli'] = 'Pandaren Alliance';
$lang['race_panda_horde'] = 'Pandaren Horde';
$lang['race_nightborde'] = 'Nightborne';
$lang['race_void_elf'] = 'Void Elf';
$lang['race_lightforged_draenei'] = 'Lightforged Draenei';
$lang['race_highmountain_tauren'] = 'Highmountain Tauren';
$lang['race_dark_iron_dwarf'] = 'Dark Iron Dwarf';
$lang['race_maghar_orc'] = 'Maghar Orc';

/*Header Lang*/
$lang['header_cookie_message'] = 'Denne nettsiden bruker informasjonskapsler for å sikre at du får den beste opplevelsen på vår hjemmeside.';
$lang['header_cookie_button'] = 'Har det!';

/*Footer Lang*/
$lang['footer_rights'] = 'Alle Rettigheter Reservert';

/*Page 404 Lang*/
$lang['page_404_title'] = '404 Side Ikke Funnet';
$lang['page_404_description'] = 'Det ser ut til at siden du leter etter ikke kunne bli funnet';

/*Panel Lang*/
$lang['panel_acc_rank'] = 'Konto Nivå';
$lang['panel_dp'] = 'DP';
$lang['panel_vp'] = 'VP';
$lang['panel_expansion'] = 'Ekspansjon';
$lang['panel_member'] = 'Medlem siden';
$lang['panel_chars_list'] = 'Karakter Liste';
$lang['panel_account_details'] = 'Konto Detaljer';
$lang['panel_last_ip'] = 'Siste IP';
$lang['panel_change_email'] = 'Endre Epost Addresse';
$lang['panel_change_password'] = 'Endre Passord';
$lang['panel_replace_pass_by'] = 'Endre passord Med';
$lang['panel_current_email'] = 'Nåværende Epost Addresse';
$lang['panel_replace_email_by'] = 'Endre Epost Med';

/*Home Lang*/
$lang['home_latest_news'] = 'Seneste Nyheter';
$lang['home_discord'] = 'Discord';
$lang['home_server_status'] = 'Server Status';
$lang['home_realm_info'] = 'For øyeblikket er serveren';

/*PvP Statistics Lang*/
$lang['statistics_top_20'] = 'TOP 20';
$lang['statistics_top_2v2'] = 'TOP 2V2';
$lang['statistics_top_3v3'] = 'TOP 3V3';
$lang['statistics_top_5v5'] = 'TOP 5V5';

/*News Lang*/
$lang['news_recent_list'] = 'Nylig nyhetsliste';
$lang['news_comments'] = 'Kommentarer';

/*Bugtracker Lang*/
$lang['bugtracker_report_notfound'] = 'Rapporter ikke funnet';

/*Donate Lang*/
$lang['donate_get'] = 'Få';

/*Vote Lang*/
$lang['vote_next_time'] = 'Neste Stemme Om:';

/*Forum Lang*/
$lang['forum_posts_count'] = 'Innlegg';
$lang['forum_topic_locked'] = 'Dette emnet er låst.';
$lang['forum_comment_locked'] = 'Har du noe å si? Logg inn for å delta i samtalen.';
$lang['forum_comment_header'] = 'Bli med i samtalen';
$lang['forum_not_authorized'] = 'Ikke autorisert';
$lang['forum_post_history'] = 'Vis innleggshistorie';
$lang['forum_topic_list'] = 'Emneliste';
$lang['forum_last_activity'] = 'Siste aktivitet';
$lang['forum_last_post_by'] = 'Siste innlegg av';
$lang['forum_whos_online'] = 'Hvem er Pålogget';
$lang['forum_replies_count'] = 'Svar';
$lang['forum_topics_count'] = 'Emner';
$lang['forum_users_count'] = 'Brukere';

/*Store Lang*/
$lang['store_categories'] = 'Butikk Kategorier';
$lang['store_top_items'] = 'TOP Utstyr';
$lang['store_cart_added'] = 'Du har lagt til';
$lang['store_cart_in_your'] = 'i din handlekurv';
$lang['store_cart_no_items'] = 'Du har ingenting i din handlekurv.';

/*Soap Lang*/
$lang['soap_send_subject'] = 'Butikk Kjøp';
$lang['soap_send_body'] = 'Takk for at du handler!';

/*Email Lang*/
$lang['email_password_recovery'] = 'Passordgjenoppretting';
$lang['email_account_activation'] = 'Kontoaktivering';
